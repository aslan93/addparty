<?php

namespace app\components\JWTEncrypt;

use Firebase\JWT\JWT;
use Yii;

class JWTEncrypt
{
    public $data        = '';
    public $secretKey   = '';
    public $algorithm   = 'HS512';
    public $encrypted   = '';
    public $decrypted   = '';

    public function encrypt($data = false)
    {
        if ((string)$data){
            $this->data = (string)$data;
        }
        $this->secretKey = Yii::$app->params['jwtSecretKey'];
        $this->encrypted =  JWT::encode([$this->data], $this->secretKey,$this->algorithm);
        return $this->encrypted;
    }

    public function decrypt($data=false)
    {
        if ((string)$data){
            $this->encrypted = (string)$data;
        }
        $this->secretKey = Yii::$app->params['jwtSecretKey'];
        $this->decrypted = JWT::decode($this->encrypted,$this->secretKey,[$this->algorithm]);
        return $this->decrypted;
    }
}
