<?php

namespace app\components;

use Yii;
use yii\base\Module;


class AdminModule extends Module
{

    public $controllerNamespace = 'app\modules\Admin\controllers';

    public function init()
    {
        $this->layoutPath = Yii::getAlias('@app/views/layouts');
        
        parent::init();
    }

}
