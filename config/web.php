<?php
use app\components\JWTEncrypt\JWTEncrypt;
use yii\web\Request;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$baseUrl = str_replace('/web', '', (new Request)->getBaseUrl());
$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
             'baseUrl'=>$baseUrl,
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '_uO56fUazAFj9jkJQFGdIu4blGroDhsu',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'response' => [
            'formatters' => [
                \yii\web\Response::FORMAT_JSON => [
                    'class' => 'yii\web\JsonResponseFormatter',
                    'prettyPrint' => YII_DEBUG, // use "pretty" output in debug mode
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                ],
            ],
//            'on afterPrepare' => function ($event) {
//                $response = $event->sender;
//                if ($response->content && $response->format == \yii\web\Response::FORMAT_JSON ) {
//                    $jwt = new JWTEncrypt();
//                    $response->content = $jwt->encrypt($response->content);
//                }
//            }

        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\modules\User\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mail',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp-relay.sendinblue.com',
                'username' => 'peoplefinder@nomadroot.com',
                'password' => '1LHDhMJxGCAVfRtb',
                'port' => '587',
                //'encryption' => 'tls',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'urlManager' => [
            'class' => 'app\components\CustomUrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                ['class' => 'yii\rest\UrlRule',
                    'controller' => 'phone',
                ],
            ],
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'app\modules\User\User',
            'defaultRoute' => 'user/index',
        ],
        'report' => [
            'class' => 'app\modules\Report\Report',
            'defaultRoute' => 'report/index',
        ],
        'boost-package' => [
            'class' => 'app\modules\BoostPackage\BoostPackage',
            'defaultRoute' => 'boost-package/index',
        ],
        'coin-package' => [
            'class' => 'app\modules\CoinPackage\CoinPackage',
            'defaultRoute' => 'coin-package/index',
        ],
        'tag' => [
            'class' => 'app\modules\Tag\Tag',
            'defaultRoute' => 'tag/index',
        ],

        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}
return $config;
