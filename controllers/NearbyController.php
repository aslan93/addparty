<?php


namespace app\controllers;

use app\modules\User\models\User;
use yii\data\Pagination;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;

class NearbyController extends ActiveController
{
    public $modelClass = 'app\modules\Tag\models\Tag';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'validate' => ['GET'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH', 'POST'],
            'delete' => ['DELETE'],
            'upload' => ['POST'],
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'index' => null,
            'create' => null, // Disable create
            'view' => null,
            'update' => null,
            'validate' => null,
            'upload' => null,
        ]);
    }

    public function actionGet($id){
        $user = User::find()->where(['ID'=>$id])->with(['settings'])->one();
        $lineUsers = User::find()->joinWith(['status','settings'])->with('status');
        // not find me
        $lineUsers->where(['<>','User.ID',$id])->andWhere(['City'=>$user->City,'Country'=>$user->Country]);

        //Look for filter by Gender
        if ($user->settings->LFGender){
            switch ($user->settings->LFGender){
                case 0: break;
                case 1: break;
                case 2:
                    $lineUsers->andWhere(['User.Gender'=>'Boy']);
                    break;
                case 3:
                    $lineUsers->andWhere(['User.Gender'=>'Girl']);
                    break;
                case 4:
                    $lineUsers->andWhere(['User.Interested'=>'Girls']);
                    break;
                case 5:
                    $lineUsers->andWhere(['User.Interested'=>'Boys']);
                    break;
                case 6:
                    $lineUsers->andWhere(['User.GenderCode'=>6]);
                    break;
                case 7:
                    $lineUsers->andWhere(['User.GenderCode'=>7]);
                    break;
                case 8:
                    $lineUsers->andWhere(['User.GenderCode'=>8]);
                    break;
                case 9:
                    $lineUsers->andWhere(['User.GenderCode'=>9]);
                    break;
                default:
                    break;
            }
        }

        //Look for filter by Social Account
        if ($user->settings->SocialAccount){
            $accounts = explode(',',$user->settings->SocialAccount);
            if (count($accounts) === 3) {
                $lineUsers->andWhere(['or', ['IS NOT', "User.$accounts[0]", null],['IS NOT', "User.$accounts[1]", null],['IS NOT', "User.$accounts[2]", null]]);
            }elseif (count($accounts) === 2){
                $lineUsers->andWhere(['or', ['IS NOT', "User.$accounts[0]", null],['IS NOT', "User.$accounts[1]", null]]);
            }elseif (count($accounts) === 1){
                $lineUsers->andWhere(['or', ['IS NOT', "User.$accounts[0]", null]]);
            }
        }

        // filter per AGE
        if ($user->settings->LFAgeFrom > 0 ){
            $lineUsers->andWhere(['>=','User.Age',$user->settings->LFAgeFrom]);
        }

        if ($user->settings->LFAgeTo > 0){
            $lineUsers->andWhere(['<=','User.Age',$user->settings->LFAgeTo]);
        }

        // Who sees me filter
        if ($user->Age > 0){
            $lineUsers->andWhere(['<=','UserSettings.WSAgeFrom',$user->Age]);
            $lineUsers->andWhere(['>=','UserSettings.WSAgeTo',$user->Age]);
        }

        $countQuery = clone $lineUsers;
        $countq = $countQuery->count();
        $rounding = 0.5;
        $pageCount = $countq/20;
        $pageCount = round(ceil($pageCount/$rounding)*$rounding);

        $pages = new Pagination(['totalCount' => $countq,'pageSize'=>20]);

        if ( Yii::$app->request->get('page') > $pageCount){
            return [];
        }

        // order by CreatedAt
        $users = $lineUsers->offset($pages->offset)->limit($pages->limit)->all();
        $unlocked = $user->unlockedIDS;
        if ($users) {
            foreach ($users as $key => $user_item) {
                if (in_array($user_item->ID,$unlocked)){
                    $user_item->Unlocked = 1;
                }else{
                    $user_item->Unlocked = 0;
                }
                $user_item->AccountStatus = $user_item->status;
                $likedIDs = json_decode(unserialize($user_item->AccountStatus->LikedMe),true);
                if (isset($likedIDs[0])) {
                    $newLikes = $likedIDs[0];
                    unset($likedIDs[0]);
                }
                if (count($likedIDs) > 0) {

                    $lkd = $likedIDs;
                    unset($lkd[0]);
                    if (in_array($id,$lkd)) {
                        $user_item->AccountStatus->ILiked = 1;
                    } else {
                        $user_item->AccountStatus->ILiked = 0;
                    }
                }else{
                    $user_item->AccountStatus->ILiked = 0;
                }
                switch ($user_item->settings->WSGender){
                    case 'Boys':
                        if ($user->Gender !== 'Boy'){
                            unset($users[$key]);
                        }
                        break;
                    case 'Girls':
                        if ($user->Gender !== 'Girl'){
                            unset($users[$key]);
                        }
                        break;
                    case 'All':
                        break;
                }
            }
        }
        return array_values($users);
    }
}
