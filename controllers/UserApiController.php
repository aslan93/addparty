<?php


namespace app\controllers;

use app\modules\Report\models\Report;
use app\modules\User\models\UserBlock;
use app\modules\User\models\UserSettings;
use app\modules\User\models\UserStatus;
use yii\helpers\Url;
use yii\web\UploadedFile;
use app\modules\User\models\User;
use app\modules\User\models\UserPhoto;
use yii\imagine\Image;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;


class UserApiController extends ActiveController
{
    public $modelClass = 'app\modules\User\models\User';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',

    ];

    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'validate' => ['GET'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH', 'POST'],
            'delete' => ['DELETE'],
            'upload' => ['POST'],
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'index' => null,
            'create' => null, // Disable create
            'view' => null,
            'update' => null,
            'validate' => null,
            'upload' => null,
        ]);
    }

    public function actionRegister()
    {
        $user = new User();
        $user->Encrypt = true;
        $user->UserName = \Yii::$app->request->post('UserName');
        $user->Phone = \Yii::$app->request->post('Phone');
        $user->Password = 'user';
        $user->Status = 'Active';
        $user->Type = 'User';
        $user->CreatedAt = time();
        if ($user->save()) {
            $this->saveImages($user->ID);
            $userStatus = new UserStatus(['UserID'=>$user->ID]);
            $userStatus->save();
            $userSettings = new UserSettings(['UserID'=>$user->ID]);
            $userSettings->save();
            $this->redirect(Url::to(['/user-api/get-user','id'=>$user->ID]));
            return $user;
        }
        return 0;
    }

    public function actionGetUser($id,$vid=0){

        $userModel = User::find()->where(['ID'=>$id])->with(['userPhotos','userTags.tag','settings','status'])->one();
        if (!$userModel){
            return ['Response'=>"User with id $id not exists!"];
        }

        $userModel->Photos = $userModel->userPhotos;
        if ($vid > 0){
            foreach ($userModel->blockedUsers as $buser){
                if ($buser['BlockedUserID'] === $vid){
                    return ['response'=>0,'message'=>'You are blocked by user '.$id.' !'];
                }
            }
        }

        // user tags
        $utags = $userModel->userTags;
        $tags=[];
        foreach ($utags as $tag){
            $tags[] = $tag->tag;
        }
        $userModel->Tags = $tags;

        //settings
        $userModel->AccountStatus = $userModel->status;
        $userModel->Settings = $userModel->settings;

        if ($vid){
            //Viewed Me
            $user = UserStatus::find()->where(['UserID'=>$id])->one();
            if ($user){
                $viewed_me = [];
                if ($user->ViewedMe){
                    $viewed_me = json_decode(unserialize($user->ViewedMe),true);
                }
                if(!in_array((int)$vid, $viewed_me)){
                    if (isset($viewed_me[0]['newViews'])){
                        $viewed_me[0]['newViews'] += 1;
                    }else{
                        $viewed_me[0]['newViews'] = 1;
                    }
                    $viewed_me = array_merge($viewed_me,[(int)$vid]);
                    $user->ViewedMe = serialize(json_encode($viewed_me));
                    $user->Views += 1;
                    $user->save();
                }else{
                    $n_viewed_me = $viewed_me;
                    unset($n_viewed_me[0]);
                    $n_viewed_me = array_diff($n_viewed_me, [(int)$vid]);
                    $n_viewed_me = array_merge($n_viewed_me,[(int)$vid]);
                    $viewed_me = array_merge([$viewed_me[0]],$n_viewed_me);
                    $user->ViewedMe = serialize(json_encode($viewed_me));
                    $user->Views += 1;
                    $user->save();
                }
            }
        }

        // users viewed me
        $viewedIDs = json_decode(unserialize($userModel->AccountStatus->ViewedMe),true);
        $newViews = $viewedIDs[0];
        unset($viewedIDs[0]);
        $userModel->AccountStatus->NewViews = 0;
        if (count($viewedIDs) > 0){
        $viewedUsers = User::find()->where(['ID'=>$viewedIDs])
            ->orderBy([new \yii\db\Expression('FIELD (ID, ' . implode(',', array_reverse($viewedIDs)) . ')')])
            ->limit(3)->all();
        $vphotos = [];
        foreach ($viewedUsers as $us){
            if ($us->Photos){
                $vphotos[] = $us->Photos[0]->Small;
            }else{
                $vphotos[] = 'default.jpg';
            }

        }
        if (count($vphotos)<3){
            $ct = 3 - count($vphotos);
            for ($i=1;$i<=$ct;$i++){
                $vphotos[] = 'default.jpg';
            }
        }
        $userModel->AccountStatus->ViewedMe = array_reverse($vphotos);
        $userModel->AccountStatus->NewViews = isset($newViews['newViews'])?$newViews['newViews']:0;
        }else{
            $userModel->AccountStatus->ViewedMe = ['default.jpg','default.jpg','default.jpg'];
        }

        //liked users
        $likedIDs = json_decode(unserialize($userModel->AccountStatus->LikedMe),true);
        if (isset($likedIDs[0])) {
            $newLikes = $likedIDs[0];
            unset($likedIDs[0]);
        }
        if (count($likedIDs) > 0) {

            $lkd = $likedIDs;
            unset($lkd[0]);
            if (in_array($vid,$lkd)) {
                $userModel->AccountStatus->ILiked = 1;
            } else {
                $userModel->AccountStatus->ILiked = 0;
            }
        }else{
            $userModel->AccountStatus->ILiked = 0;
        }
        $userModel->AccountStatus->NewLikes = 0;
        if (count($likedIDs) > 0) {
            $likedUsers = User::find()->where(['ID' => $likedIDs])
                ->orderBy([new \yii\db\Expression('FIELD (ID, ' . implode(',', array_reverse($likedIDs)) . ')')])
                ->limit(3)->all();
            $lphotos = [];
            foreach ($likedUsers as $us){
                if ($us->Photos){
                   $lphotos[] = $us->Photos[0]->Small;
                }else{
                    $lphotos[] = 'default.jpg';
                }
            }
            if (count($lphotos)<3){
               $ct = 3 - count($lphotos);
               for ($i=1;$i<=$ct;$i++){
                   $lphotos[] ='default.jpg';
               }
            }
            $userModel->AccountStatus->LikedMe = array_reverse($lphotos);
            $userModel->AccountStatus->NewLikes = isset($newLikes['newLikes'])?$newLikes['newLikes']:'0';
        }else{
            $userModel->AccountStatus->LikedMe = ['default.jpg','default.jpg','default.jpg'];
        }

        // users that i like
        $melikedIDs = json_decode(unserialize($userModel->AccountStatus->MyLikes),true);
        if ($melikedIDs) {
            $melikedUsers = User::find()->where(['ID' => $melikedIDs])
                ->orderBy([new \yii\db\Expression('FIELD (ID, ' . implode(',', array_reverse($melikedIDs)) . ')')])
                ->limit(3)->all();
            $mlphotos = [];
            foreach ($melikedUsers as $us){

                if ($us->Photos){
                    $mlphotos[] = $us->Photos[0]->Small;
                }else{
                    $mlphotos[] = 'default.jpg';
                }
            }
            if (count($mlphotos)<3){
                $ct = 3 - count($mlphotos);
                for ($i=1;$i<=$ct;$i++){
                    $mlphotos[] ='default.jpg';
                }
            }
            $userModel->AccountStatus->MyLikes = array_reverse($mlphotos);
        }else{
            $userModel->AccountStatus->MyLikes = ['default.jpg','default.jpg','default.jpg'];
        }

        $userModel->Earned = $userModel->earned;
        return $userModel;
    }

    public function actionUpdate($id,$authkey)
    {   $data['User'] =  \Yii::$app->request->post();
        $user = User::findOne($id);
        if ($user && $user->validateAuthKey($authkey)){
            if ($user->load($data) ) {
                unset($user->AuthKey);
                if ($user->save()){
                    $this->redirect(Url::to(['/user-api/get-user','id'=>$user->ID]));
                }
            }
        }
        return 0;
    }

    public function actionUploadImages($id){
        return $this->saveImages($id);
    }

    public function actionRemoveImage($id){
        return ['response'=>UserPhoto::deleteAll(['ID'=>$id])];
    }

    public function actionSetMainImage($uid,$pid){
        return $this->setMainImage($uid,$pid);
    }

    public function setMainImage($uid,$pid){
        $photos = UserPhoto::find()->where(['UserID'=>$uid])->all();
        foreach ($photos as $photo){
            $photo->IsMain = ($photo->ID == $pid)?1:0;
            $photo->save();
        }
        return ['response'=>1];
    }

    public function actionPassword($id,$authkey)
    {
        $user = User::findOne($id);
        if ($user && $user->AuthKey === $authkey){
            $user->Encrypt = true;
            $user->Password = \Yii::$app->request->post('Password');
            if ($user->save(false)) {
                return $user;
            }
        }
        return 0;
    }

    public function actionLogin()
    {
        $login = \Yii::$app->request->post('UserName');
        $phone = \Yii::$app->request->post('Phone');
        if ($login){
            $user =  User::findByUsername($login);
            if ($user){
                $this->redirect(Url::to(['/user-api/get-user','id'=>$user->ID]));
                return $user;
            }else{
                return 0;
            }

        }elseif ($phone){
            $user =  User::findByPhone($phone);
            if ($user){
                $this->redirect(Url::to(['/user-api/get-user','id'=>$user->ID]));
                return $user;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }

    public function saveImages($uid){
        $uploads = UploadedFile::getInstancesByName("Images");
        if (empty($uploads)){
            return 'no files';
        }
        $mainPid = 0;
        $photos = UserPhoto::findAll(['UserID'=>$uid]);
        foreach ($photos as $photo){
            $photo->delete();
        }
        if($uploads){
            $count = count($uploads);
            foreach ($uploads as $key => $image)
            {
                $file = md5(microtime(true)) . '.' . $image->extension;
                if ($image->saveAs(\Yii::getAlias("@webroot/uploads/users/$file")))
                {
                    Image::thumbnail(Yii::getAlias("@webroot/uploads/users/$file"), 400, 600)->save(Yii::getAlias("@webroot/uploads/users/medium_$file"));
                    Image::thumbnail(Yii::getAlias("@webroot/uploads/users/$file"), 200, 300)->save(Yii::getAlias("@webroot/uploads/users/small_$file"));
                    $imageModel = new UserPhoto(['UserID'=>$uid,'Large'=>$file,'Medium'=>'medium_'.$file,'Small'=>'small_'.$file]);
                    $imageModel->save();
                    if ($key === 0){
                        $mainPid = $imageModel->ID;
                    }
                }
            }

            if ($mainPid > 0) {
                $res = $this->setMainImage($uid, $mainPid);
            }
            return ['saved'=>$count.' files'];
        }
    }

    public function actionSettings(){
        $data['UserSettings'] = Yii::$app->request->post();
        $userSettings = UserSettings::find()->where(['UserID'=>$data['UserSettings']['UserID']])->one();
        if (!$userSettings){
            $userSettings = new UserSettings();
        }
        if ($userSettings->load($data) && $userSettings->save()){
            return $userSettings;
        }else{
            return $userSettings;
        }
    }

    public function actionStatus(){
        $data['UserStatus'] = Yii::$app->request->post();
        $userStatus = UserStatus::find()->where(['UserID'=>$data['UserStatus']['UserID']])->one();
        if (!$userStatus){
            $userStatus = new UserStatus();
        }
        if ($userStatus->load($data) && $userStatus->save()){
            return $userStatus;
        }else{
            return $userStatus;
        }
    }

    public function actionReport(){
        $data = \Yii::$app->request->post();
        $report = new Report([
            'VictimID'=>$data['VictimID'],
            'Type' => $data['Type'],
            'EntityID' => $data['UserID'],
        ]);
        if ($report->save()){
            return ['response'=>1];
        }
        return ['response'=>0];
    }

    public function actionBlock($uid,$buid){
        $block = UserBlock::find()->where(['UserID'=>$uid,'BlockedUserID'=>$buid])->one();
        if (!$block){
            $block = new UserBlock(['UserID'=>$uid,'BlockedUserID'=>$buid]);
            if ($block->save()){
                return ['response'=>1];
            }
        }
        return ['response'=>0];
    }

    public function actionUnblock($uid,$buid){
        $block = UserBlock::deleteAll(['UserID'=>$uid,'BlockedUserID'=>$buid]);
        return ['response'=>$block];
    }

    public function actionDeleteAccount($id,$authkey){
        return ['Response'=>User::deleteAll(['ID'=>$id,'AuthKey'=>$authkey])];
    }

    public function actionAddViews($uid,$quantity){
        $user = UserStatus::find()->where(['UserID'=>$uid])->one();
        $user->Views += $quantity;
        $user->save();
        return ['Views'=>$user->Views];
    }

    public function actionGenerate($count){
        $c = 0;
        for ($i = 0;$i < $count;$i++ ){
            $user = new User();
            $user->UserName = 'User-'.($i+1);
            $user->Phone = '3736945278'.$i;
            $user->Password = 'admin';
            $user->Status = 'Active';
            $user->Type = 'User';
            $user->CreatedAt = time();
            $user->Encrypt = true;
            $user->Kik = mt_rand(0,5) === 1?'vartanov':null;
            $user->Snapchat = mt_rand(0,3) === 1?'vartanov':null;
            if (!$user->Kik && !$user->Snapchat){
                $user->Instagram ='vartanov';
            }
            $user->Gender = mt_rand(0,1) === 1?'Boy':'Girl';
            $user->Interested = mt_rand(0,1) === 1?'Girls':'Boys';
            $user->Age = mt_rand(12,80);
            $user->Bio = 'Biography';
            switch (mt_rand(1,4)){
                case 1:
                    $user->Country = 'Moldova';
                    $user->City = 'Chisinau';
                    break;
                case 2:
                    $user->Country = 'Russia';
                    $user->City = 'Moscow';
                    break;
                case 3:
                    $user->Country = 'USA';
                    $user->City = 'New York';
                    break;
                case 4:
                    $user->Country = 'France';
                    $user->City = 'Paris';
                    break;
            }
            $user->save(false);
            $userStatus = new UserStatus(['UserID'=>$user->ID]);
            $userStatus->save();
            $userSettings = new UserSettings(['UserID'=>$user->ID]);
            $userSettings->save();

            switch ($user->Gender){
                case 'Boy':
                    switch ($user->Interested){
                        case 'Boys':
                            $user->GenderCode = 8;
                            $userSettings->LFGender = 8;
                            break;
                        case 'Girls':
                            $user->GenderCode = 9;
                            $userSettings->LFGender = 6;
                            break;
                    }
                    break;
                    
                case 'Girl':
                    switch ($user->Interested){
                        case 'Boys':
                            $user->GenderCode = 6;
                            $userSettings->LFGender = 9;
                            break;
                        case 'Girls':
                            $user->GenderCode = 7;
                            $userSettings->LFGender = 7;
                            break;
                    }
                    break;
            }

            $user->save(false);
            $userSettings->save(false);
            $userStatus->Vip = mt_rand(0,1);
            $userStatus->Coins = mt_rand(10,1000);
            $userStatus->Boost = mt_rand(0,10000);
            $userStatus->BoostedAt = time();
            $userStatus->Likes = mt_rand(0,1000000);
            $userStatus->Views = mt_rand(0,1000000);
            $userStatus->save(false);

            $userPhoto = new UserPhoto();
            $userPhoto->Large = '710d0b8f57971d4900a6cfd0e995fc36.jpg';
            $userPhoto->Medium = 'medium_710d0b8f57971d4900a6cfd0e995fc36.jpg';
            $userPhoto->Small = 'small_710d0b8f57971d4900a6cfd0e995fc36.jpg';
            $userPhoto->IsMain = 1;
            $userPhoto->UserID = $user->ID;
            $userPhoto->save(false);

            $c += 1;
        }

        return ['genered'=>$c];
    }

    public function actionAddView($id,$vid){
        if ($vid){
            //Viewed Me
            $user = UserStatus::find()->where(['UserID'=>$id])->one();
            if ($user){
                $viewed_me = [];
                if ($user->ViewedMe){
                    $viewed_me = json_decode(unserialize($user->ViewedMe),true);
                }
                if(!in_array((int)$vid, $viewed_me)){
                    if (isset($viewed_me[0]['newViews'])){
                        $viewed_me[0]['newViews'] += 1;
                    }else{
                        $viewed_me[0]['newViews'] = 1;
                    }
                    $viewed_me = array_merge($viewed_me,[(int)$vid]);
                    $user->ViewedMe = serialize(json_encode($viewed_me));
                    $user->Views += 1;
                    $user->save();
                    return 1;
                }else{
                    $n_viewed_me = $viewed_me;
                    unset($n_viewed_me[0]);
                    $n_viewed_me = array_diff($n_viewed_me, [(int)$vid]);
                    $n_viewed_me = array_merge($n_viewed_me,[(int)$vid]);
                    $viewed_me = array_merge([$viewed_me[0]],$n_viewed_me);
                    $user->ViewedMe = serialize(json_encode($viewed_me));
                    $user->Views += 1;
                    $user->save();
                    return 1;
                }
            }
        }
    }
}
