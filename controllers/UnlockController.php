<?php


namespace app\controllers;

use app\modules\Tag\models\Tag;
use app\modules\Tag\models\UserTag;
use app\modules\User\models\EarnCoin;
use app\modules\User\models\UserStatus;
use app\modules\User\models\UserUnlock;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;


class UnlockController extends ActiveController
{
    public $modelClass = 'app\modules\User\models\UserUnlock';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'validate' => ['GET'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH', 'POST'],
            'delete' => ['DELETE'],
            'upload' => ['POST'],
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'index' => null,
            'create' => null, // Disable create
            'view' => null,
            'update' => null,
            'validate' => null,
            'upload' => null,
        ]);
    }

    // unlock the user
    public function actionAdd($id,$uid){
        $uuser = UserUnlock::find()->where(['UserID'=>$id,'UnlockedID'=>$uid])->one();
        if (!$uuser) {

            $uuser = new UserUnlock(['UserID' => $id, 'UnlockedID' => $uid]);
            if ($uuser->save()){
                return ['response' => 1];
            }else{
                return ['response' => 0];
            }

        }else{
            return ['response' => 0];
        }
    }

    // check the user if is unlocked
    public function actionCheck($id,$uid){
        $uuser = UserUnlock::find()->where(['UserID'=>$id,'UnlockedID'=>$uid])->one();
        if (!$uuser) {
            return ['response' => 0];
        }else{
            return ['response' => 1];
        }
    }

}
