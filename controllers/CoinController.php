<?php


namespace app\controllers;

use app\modules\Tag\models\Tag;
use app\modules\Tag\models\UserTag;
use app\modules\User\models\EarnCoin;
use app\modules\User\models\UserStatus;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;


class CoinController extends ActiveController
{
    public $modelClass = 'app\modules\Tag\models\Tag';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'validate' => ['GET'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH', 'POST'],
            'delete' => ['DELETE'],
            'upload' => ['POST'],
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'index' => null,
            'create' => null, // Disable create
            'view' => null,
            'update' => null,
            'validate' => null,
            'upload' => null,
        ]);
    }

    // add coins to User
    public function actionAdd($id,$quantity,$authkey){
        $user = UserStatus::find()->with('user')->where(['UserID'=>$id])->one();
        if ($user && $user->user->validateAuthKey($authkey)){
            $user->Coins += $quantity;
            if ($user->save()){
                return ['coins'=>$user->Coins];
            }else{
                return ['coins'=>$user->Coins];
            }
        }else{
            return ['Response'=>'Invalid Auth Key'];
        }

    }

    // earn coins to User
    public function actionAddEarn($id,$quantity,$authkey){
        $user = UserStatus::find()->with('user')->where(['UserID'=>$id])->one();
        if ($user && $user->user->validateAuthKey($authkey)){
            $user->Coins += $quantity;
            $earn = EarnCoin::find()->where(['UserID'=>$id])->one();
            if (!$earn){
                $earn = new EarnCoin();
                $earn->UserID = $id;
                $earn->TotalEarned = $quantity;
                $earn->LastEarnCount = $quantity;
                $earn->LastEarnTime = time();
            }else{
                $earn->TotalEarned += $quantity;
                $current = strtotime(date("Y-m-d"));
                $date    = strtotime(date("Y-m-d",$earn->LastEarnTime));
                $datediff = $date - $current;
                $difference = $datediff;
                if($difference === 0)
                {
                    $earn->LastEarnCount += $quantity;
                }else{
                    $earn->LastEarnCount = $quantity;
                }
                $earn->LastEarnTime = time();
            }

            if ($user->save() && $earn->save()){
                return ['total_earned'=>$earn->TotalEarned,'today_earned'=>$earn->LastEarnCount];
            }else{
                return ['total_earned'=>$earn->TotalEarned,'today_earned'=>$earn->LastEarnCount];
            }
        }else{
            return ['Response'=>'Invalid Auth Key'];
        }

    }

    // put out coins from User
    public function actionOut($id,$quantity,$authkey){
        $user = UserStatus::find()->where(['UserID'=>$id])->one();
        if ($user && $user->user->validateAuthKey($authkey) ) {
            $user->Coins -= $quantity;
            if ($user->save()) {
                return ['coins' => $user->Coins];
            } else {
                return ['coins' => $user->Coins];
            }
        }else{
            return ['Response' => 'Invalid Auth Key'];
        }
    }

    public function actionEarns($id){
        $earn = EarnCoin::find()->where(['UserID'=>$id])->one();
        if ($earn) {
            $total = $earn->TotalEarned;
            $current = strtotime(date("Y-m-d"));
            $date = strtotime(date("Y-m-d", $earn->LastEarnTime));
            $datediff = $date - $current;
            $difference = $datediff ;
            if ($difference === 0) {
                $today = $earn->LastEarnCount;
            } else {
                $today = 0;
            }
        }else{
            $total = 0;
            $today = 0;
        }

        return ['total_earned'=>$total,'today_earned'=>$today];
    }
}
