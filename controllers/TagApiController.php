<?php


namespace app\controllers;

use app\modules\Tag\models\Tag;
use app\modules\Tag\models\UserTag;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;


class TagApiController extends ActiveController
{
    public $modelClass = 'app\modules\Tag\models\Tag';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'validate' => ['GET'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH', 'POST'],
            'delete' => ['DELETE'],
            'upload' => ['POST'],
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'index' => null,
            'create' => null, // Disable create
            'view' => null,
            'update' => null,
            'validate' => null,
            'upload' => null,
        ]);
    }

    //get all tags from system
    public function actionIndex(){
        return Tag::find()->all();
    }

    // add tag in system and assign it to User
    public function actionCreateTag(){
        $data['Tags'] = Yii::$app->request->post('Tags');
        $existent = [];
        foreach ($data['Tags'] as $t){
            $tag = Tag::find()->where(['Name'=>$t])->one();
            if ($tag){
                $existent[] = $tag;
            }
            $tag = new Tag(['Name'=>$t]);
            if($tag->save()){
                $existent[] = $tag;
            }
        }
        return $existent;

    }

    // Add tag to user
    public function actionAddTag($uid){
        $tags = Yii::$app->request->post('Tags');
        UserTag::deleteAll(['UserID'=>$uid]);
        foreach ($tags as $t){
            $tag = new UserTag(['UserID'=>$uid,'TagID'=>$t]);
            $tag->save();
        }
        return ['response'=>1];
    }

    // Remove Tag from User
    public function actionRemoveTag($uid,$tid){
        $userTag = UserTag::find()->where(['UserID'=>$uid,'TagID'=>$tid])->one();
        if ($userTag){
            $userTag->delete();
        }
        return ['response'=>1];
    }

    //Delete tag from system
    public function actionDeleteTag($id){
        $tag = Tag::findOne($id);
        if ($tag){
            return ['response'=>$tag->delete()];
        }else{
            return ['response'=>0];
        }
    }
}
