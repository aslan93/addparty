<?php


namespace app\controllers;

use app\modules\BoostPackage\models\BoostPackage;
use app\modules\CoinPackage\models\CoinPackage;
use app\modules\Tag\models\Tag;
use app\modules\Tag\models\UserTag;
use app\modules\User\models\UserStatus;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;


class PackageController extends ActiveController
{
    public $modelClass = 'app\modules\Tag\models\Tag';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'validate' => ['GET'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH', 'POST'],
            'delete' => ['DELETE'],
            'upload' => ['POST'],
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'index' => null,
            'create' => null, // Disable create
            'view' => null,
            'update' => null,
            'validate' => null,
            'upload' => null,
        ]);
    }

    // get packages
    public function actionGetAll(){

        $packages = CoinPackage::find()->all();
        $result = [];
        foreach ($packages as $key => $package){

            if ($package->Vip === 1){
                $result['Vip'][] = $package;
                continue;
            }

            if ($package->Price === '0.00'){
                $result['Free'][] = $package;
            }else{
                $result['Regular'][] = $package;
            }

        }

        return $result;

    }

    public function actionGetBoostPackages(){
        return BoostPackage::find()->all();
    }

}
