<?php


namespace app\controllers;

use app\modules\Tag\models\Tag;
use app\modules\Tag\models\UserTag;
use app\modules\User\models\User;
use app\modules\User\models\UserStatus;
use yii\data\Pagination;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;


class LikeController extends ActiveController
{
    public $modelClass = 'app\modules\Tag\models\Tag';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'validate' => ['GET'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH', 'POST'],
            'delete' => ['DELETE'],
            'upload' => ['POST'],
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'index' => null,
            'create' => null, // Disable create
            'view' => null,
            'update' => null,
            'validate' => null,
            'upload' => null,
        ]);
    }

    // add like to User
    public function actionAdd($uid,$luid){
        $err = 0;
        $luser = UserStatus::find()->where(['UserID'=>$luid])->one();
        //Liked Me
        $user = UserStatus::find()->where(['UserID'=>$uid])->one();
        if ($user){
            $liked_me = [];
            if ($user->LikedMe){
                $liked_me = json_decode(unserialize($user->LikedMe),true);
            }
            if ($luser){
                if(!in_array((int)$luid, $liked_me)){
                    if (isset($liked_me[0]['newLikes'])){
                        $liked_me[0]['newLikes'] += 1;
                    }else{
                        $liked_me[0]['newLikes'] = 1;
                    }
                    $liked_me = array_merge($liked_me,[(int)$luid]);
                    $user->LikedMe = serialize(json_encode($liked_me));
                    $user->Likes += 1;
                    $user->save();
                }
            }
        }else{
            $err +=1;
        }

        //My Likes
        if ($luser){
            $i_liked = [];
            if ($luser->MyLikes){
                $i_liked = json_decode(unserialize($luser->MyLikes),true);
            }
            if (!in_array((int)$uid, $i_liked)){
                $i_liked = array_merge($i_liked,[(int)$uid]);
                $luser->MyLikes = serialize(json_encode($i_liked));
                $luser->save();
            }
        }else{
            $err +=1;
        }
        return $err?0:['likes'=>$user->Likes];
    }

    public function actionUnset($uid,$luid){
        $err = 0;
        $luser = UserStatus::find()->where(['UserID'=>$luid])->one();
        //Liked Me
        $user = UserStatus::find()->where(['UserID'=>$uid])->one();
        if ($user){
            $liked_me = [];
            if ($user->LikedMe){
                $liked_me = json_decode(unserialize($user->LikedMe),true);
            }
            if ($luser){
                if(in_array((int)$luid, $liked_me)){
                    if (isset($liked_me[0]['newLikes'])){
                        $liked_me[0]['newLikes'] -= 1;
                    }else{
                        $liked_me[0]['newLikes'] = 0;
                    }
                    $reserve = $liked_me[0];
                    unset($liked_me[0]);
                    $liked_me = array_diff($liked_me,[$luid]);
                    $liked_me = array_merge($reserve,$liked_me);
                    $user->LikedMe = serialize(json_encode($liked_me));
                    $user->Likes -= 1;
                    $user->save();
                }
            }
        }else{
            $err +=1;
        }

        //My Likes
        if ($luser){
            $i_liked = [];
            if ($luser->MyLikes){
                $i_liked = json_decode(unserialize($luser->MyLikes),true);
            }
            if (in_array((int)$uid, $i_liked)){
                $i_liked = array_diff($i_liked,[$uid]);
                $luser->MyLikes = serialize(json_encode($i_liked));
                $luser->save();
            }
        }else{
            $err +=1;
        }
        return $err?0:['likes'=>$user->Likes];
    }

    public function actionGetLikedMeUsers($id){
        $userModel = UserStatus::find()->where(['UserID'=>$id])->one();
        $likedIDs = json_decode(unserialize($userModel->LikedMe));
        $userStatus = $userModel;
        if ($userStatus && $userStatus->LikedMe){
            $all = json_decode(unserialize($userStatus->LikedMe));
            $newLikes['newLikes'] = 0;
            unset($all[0]);
            $toSave = array_merge([$newLikes],$all);
            $userStatus->LikedMe = serialize(json_encode($toSave));
            $userStatus->save();
        }
        unset($likedIDs[0]);
        // pagination
        $lineUsers = User::find()->where(['ID'=>$likedIDs]);
        $countQuery = clone $lineUsers;
        $countq = $countQuery->count();
        $rounding = 0.5;
        $pageCount = $countq/20;
        $pageCount = round(ceil($pageCount/$rounding)*$rounding);
        $pages = new Pagination(['totalCount' => $countQuery->count(),'pageSize'=>20]);
        if ( Yii::$app->request->get('page') > $pageCount){
            return [];
        }
        $lineUsers->offset($pages->offset)->limit($pages->limit);
        if ($likedIDs) {
            $users = $lineUsers->orderBy([new \yii\db\Expression('FIELD (ID, ' . implode(',', array_reverse($likedIDs)) . ')')])->all();
            $user = User::find()->where(['ID'=>$id])->one();
            $unlocked = $user->unlockedIDS;
            foreach ($users as  $user_item){
                if (in_array($user_item->ID,$unlocked)){
                    $user_item->Unlocked = 1;
                }else{
                    $user_item->Unlocked = 0;
                }
            }
            return $users;
        }else{
            return [];
        }
    }

    public function actionGetViewedMeUsers($id){
        $userModel = UserStatus::find()->where(['UserID'=>$id])->one();
        $likedIDs = json_decode(unserialize($userModel->ViewedMe));
        $userStatus = $userModel;
        if ($userStatus && $userStatus->ViewedMe){
            $all = json_decode(unserialize($userStatus->ViewedMe));
            $newLikes['newViews'] = 0;
            unset($all[0]);
            $toSave = array_merge([$newLikes],$all);
            $userStatus->ViewedMe = serialize(json_encode($toSave));
            $userStatus->save();
        }
        unset($likedIDs[0]);

        // pagination
        $lineUsers = User::find()->where(['ID'=>$likedIDs]);
        $countQuery = clone $lineUsers;
        $countq = $countQuery->count();
        $rounding = 0.5;
        $pageCount = $countq/20;
        $pageCount = round(ceil($pageCount/$rounding)*$rounding);
        $pages = new Pagination(['totalCount' => $countQuery->count(),'pageSize'=>20]);
        if ( Yii::$app->request->get('page') > $pageCount){
            return [];
        }
        $lineUsers->offset($pages->offset)->limit($pages->limit);
            if ($likedIDs) {
                $users = $lineUsers->orderBy([new \yii\db\Expression('FIELD (ID, ' . implode(',', array_reverse($likedIDs)) . ')')])->all();
                $user = User::find()->where(['ID'=>$id])->one();
                $unlocked = $user->unlockedIDS;
                foreach ($users as  $user_item){
                    if (in_array($user_item->ID,$unlocked)){
                        $user_item->Unlocked = 1;
                    }else{
                        $user_item->Unlocked = 0;
                    }
                }
                return $users;
            }else{
                return [];
            }
        }

    public function actionGetLikeUsers($id){
        $userModel = UserStatus::find()->where(['UserID'=>$id])->one();
        $likedIDs = json_decode(unserialize($userModel->MyLikes));
        // pagination
        $lineUsers = User::find()->where(['ID'=>$likedIDs]);
        $countQuery = clone $lineUsers;
        $countq = $countQuery->count();
        $rounding = 0.5;
        $pageCount = $countq/20;
        $pageCount = round(ceil($pageCount/$rounding)*$rounding);
        $pages = new Pagination(['totalCount' => $countQuery->count(),'pageSize'=>20]);
        if ( Yii::$app->request->get('page') > $pageCount){
            return [];
        }
        $lineUsers->offset($pages->offset)->limit($pages->limit);
        if ($likedIDs) {
            $users = $lineUsers->orderBy([new \yii\db\Expression('FIELD (ID, ' . implode(',', array_reverse($likedIDs)) . ')')])->all();
            $user = User::find()->where(['ID'=>$id])->one();
            $unlocked = $user->unlockedIDS;
            foreach ($users as  $user_item){
                if (in_array($user_item->ID,$unlocked)){
                    $user_item->Unlocked = 1;
                }else{
                    $user_item->Unlocked = 0;
                }
            }
            return $users;
        }else{
            return [];
        }
    }
}
