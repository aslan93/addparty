<?php

namespace app\modules\Tag;

use app\components\AdminModule;

/**
 * Tag module definition class
 */
class Tag extends AdminModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Tag\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
