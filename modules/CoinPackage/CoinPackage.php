<?php

namespace app\modules\CoinPackage;

use app\components\AdminModule;
/**
 * CoinPackage module definition class
 */
class CoinPackage extends AdminModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\CoinPackage\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
