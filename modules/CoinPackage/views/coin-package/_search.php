<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\CoinPackage\models\CoinPackageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coin-package-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'Price') ?>

    <?= $form->field($model, 'Label') ?>

    <?= $form->field($model, 'Title') ?>

    <?= $form->field($model, 'InApp') ?>

    <?php // echo $form->field($model, 'Quantity') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
