<?php

use yii\helpers\Html;
use app\components\GridView\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\CoinPackage\models\CoinPackageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Coin Packages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coin-package-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Coin Package', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'ID',
            'Quantity',
            'Title',
            'Price',
            'Label',
            'InApp',
            'Icon',
            'Button',
            [
                'attribute'=>'Vip',
                'filter'=>false
            ],

            ['class' => 'app\components\GridView\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
