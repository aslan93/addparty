<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\CoinPackage\models\CoinPackage */

$this->title = 'Create Coin Package';
$this->params['breadcrumbs'][] = ['label' => 'Coin Packages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coin-package-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
