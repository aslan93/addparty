<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\CoinPackage\models\CoinPackage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coin-package-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'Title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'Label')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'Price')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'Quantity')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'InApp')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'Icon')->dropDownList(\app\modules\CoinPackage\models\CoinPackage::getIconList()) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'Button')->dropDownList(\app\modules\CoinPackage\models\CoinPackage::getButtonList()) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'Vip')->dropDownList([0=>0,1=>1]) ?>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>



    <?php ActiveForm::end(); ?>

</div>
