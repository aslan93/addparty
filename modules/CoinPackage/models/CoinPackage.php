<?php

namespace app\modules\CoinPackage\models;

use Yii;

/**
 * This is the model class for table "CoinPackage".
 *
 * @property int $ID
 * @property double $Price
 * @property string $Label
 * @property string $Title
 * @property string $InApp
 * @property int $Quantity
 */
class CoinPackage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CoinPackage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Price', 'Label', 'Title', 'Quantity'], 'required'],
            [['Price'], 'number'],
            [['Quantity','Vip'], 'integer'],
            [['Label', 'Title', 'InApp','Button','Icon'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Price' => 'Price',
            'Label' => 'Label',
            'Title' => 'Title',
            'InApp' => 'In App',
            'Quantity' => 'Quantity',
            'Button'=>'Button',
            'Icon'=>'Icon',
            'Vip'=>'Vip'
        ];
    }

    public static function getIconList(){
        return ['-'=>'-','boost'=>'boost','coins'=>'coins','lot-coins'=>'lot-coins'];
    }

    public static function getButtonList(){
        return ['Buy'=>'Buy','Invite Friends'=>'Invite Friends','Get'=>'Get','Earn'=>'Earn'];
    }
}
