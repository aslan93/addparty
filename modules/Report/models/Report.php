<?php

namespace app\modules\Report\models;

use Yii;

/**
 * This is the model class for table "Report".
 *
 * @property int $ID
 * @property string $Type
 * @property int $EntityID
 * @property string $Report
 */
class Report extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'Report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Type', 'EntityID','VictimID'], 'required'],
            [['EntityID','VictimID'], 'integer'],
            [['Type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Type' => 'Type',
            'EntityID' => 'Entity ID',
            'VictimID' => 'Victim ID',
        ];
    }
}
