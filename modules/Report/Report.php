<?php

namespace app\modules\Report;

use app\components\AdminModule;

/**
 * Report module definition class
 */
class Report extends AdminModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Report\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
