<?php

namespace app\modules\BoostPackage;

use app\components\AdminModule;
/**
 * BoostPackage module definition class
 */
class BoostPackage extends AdminModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\BoostPackage\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
