<?php

namespace app\modules\BoostPackage\models;

use Yii;

/**
 * This is the model class for table "BoostPackage".
 *
 * @property int $ID
 * @property double $Price
 * @property int $Quantity
 * @property string $Label
 * @property string $Title
 */
class BoostPackage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'BoostPackage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Price', 'Quantity', 'Label', 'Title'], 'required'],
            [['Price'], 'number'],
            [['Quantity'], 'integer'],
            [['Label', 'Title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Price' => 'Price',
            'Quantity' => 'Quantity',
            'Label' => 'Label',
            'Title' => 'Title',
        ];
    }
}
