<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\BoostPackage\models\BoostPackage */

$this->title = 'Create Boost Package';
$this->params['breadcrumbs'][] = ['label' => 'Boost Packages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="boost-package-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
