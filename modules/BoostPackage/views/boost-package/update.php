<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\BoostPackage\models\BoostPackage */

$this->title = 'Update Boost Package: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Boost Packages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Title, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="boost-package-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
