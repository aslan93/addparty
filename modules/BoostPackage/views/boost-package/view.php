<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\BoostPackage\models\BoostPackage */

$this->title = $model->Title;
$this->params['breadcrumbs'][] = ['label' => 'Boost Packages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="boost-package-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID',
            'Price',
            'Quantity',
            'Label',
            'Title',
        ],
    ]) ?>

</div>
