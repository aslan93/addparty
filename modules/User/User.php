<?php

namespace app\modules\User;


use app\components\AdminModule;


/**
 * user module definition class
 */
class User extends AdminModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\User\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
