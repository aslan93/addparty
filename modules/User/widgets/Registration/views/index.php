<?php
use kartik\popover\PopoverX;
use yii\bootstrap\Html;
use app\views\themes\rowood\assets\RowoodAssets;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;

$bundle = RowoodAssets::register($this);

?>

    <div class="reg-box">
        <?php Pjax::begin([
            'id'=> 'register-pjax',
            'enablePushState' => false,
            'enableReplaceState'=>false,
        ])?>
        <?php
        if (!$success) {
            ?>
            <?php $form = ActiveForm::begin([
                'action' => '',
                'method' => 'post',
                'id' => 'register',
                'fieldConfig' => [
                    'options' => [
                        'tag' => false,
                    ]
                ],
                'options' => [

                    'class' => 'register-pjax',
                    'data-pjax' => true,
                ]

            ]); ?>

            <div class="col-sm-6"><?= $form->field($modelInfo, 'FirstName')->textInput(['maxlength' => true]) ?></div>
            <div class="col-sm-6"><?= $form->field($modelInfo, 'LastName')->textInput(['maxlength' => true]) ?></div>
            <div class="col-sm-6"><?= $form->field($modelInfo, 'Sex')->dropDownList($modelInfo->sexType) ?></div>
            <div class="col-sm-6"><?= $form->field($modelInfo, 'Phone')->Input('number') ?></div>
            <div class="col-sm-6"><?= $form->field($model, 'Email')->textInput(['maxlength' => true]) ?></div>
            <div class="col-sm-6"><?= $form->field($model, 'Password')->passwordInput(['maxlength' => true]) ?></div>
            <div class="submit-form">
                <?= Html::SubmitButton('Inregistrare', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
            <?php
        }else{?>
            <div>Inregistrat cu success</div><br>
            <div>Acum puteti sa v-a logati cu email-ul <?=$model->Email?> si parola!</div>
            <?php
        }
        ?>
        <?php Pjax::end()?>
    </div>



