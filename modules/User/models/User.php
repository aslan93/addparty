<?php

namespace app\modules\User\models;

use Yii;
use yii\web\IdentityInterface;
use app\modules\Tag\models\UserTag;
/**
 * This is the model class for table "User".
 *
 * @property int $ID
 * @property string $UserName
 * @property string $Password
 * @property string $AuthKey
 * @property string $Status
 * @property string $Type
 * @property string $FirstName
 * @property string $LastName
 * @property string $Snapchat
 * @property string $Instagram
 * @property string $Kik
 * @property int $Age
 * @property string $Gender
 * @property string $Country
 * @property string $City
 * @property string $Bio
 * @property string $Interested
 * @property string $Photos
 * @property string $Tags
 * @property string $Encrypt
 * @property string $CreatedAt
 * @property string $AccountStatus
 * @property string $Settings
 * @property string $Unlocked
 *
 * @property UserPhoto[] $userPhotos
 * @property UserStatus[] $status
 * @property UserSettings[] $settings
 * @property UserTag[] $userTags
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    public $Encrypt = false;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'User';
    }

    public function attributes()
    {
        return array_merge(
            parent::attributes(),['Photos','Tags','AccountStatus','Settings','Earned','Unlocked']

        );
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Password'], 'required'],
            [['Earned','Status', 'Type', 'FirstName', 'LastName', 'Snapchat', 'Instagram', 'Kik', 'Age', 'Gender', 'Country', 'City', 'Bio', 'Interested','Phone','GenderCode','Unlocked'],'safe'],
            [['Age','CreatedAt'], 'integer'],
            [['Bio'], 'string'],
            [['UserName'], 'string', 'max' => 225],
            [['Password', 'AuthKey', 'Status', 'Type'], 'string', 'max' => 50],
            [['FirstName', 'LastName', 'Snapchat', 'Instagram', 'Kik', 'Gender', 'Country', 'City', 'Interested'], 'string', 'max' => 255],
            [['UserName','Phone','AuthKey'], 'unique'],
            [['Phone'],'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'UserName' => 'User Name',
            'Password' => 'Password',
            'AuthKey' => 'Auth Key',
            'Status' => 'Status',
            'Type' => 'Type',
            'FirstName' => 'First Name',
            'LastName' => 'Last Name',
            'Snapchat' => 'Snapchat',
            'Instagram' => 'Instagram',
            'Kik' => 'Kik',
            'Age' => 'Age',
            'Gender' => 'Gender',
            'Country' => 'Country',
            'City' => 'City',
            'Bio' => 'Bio',
            'Interested' => 'Interested',
            'CreatedAt'=>'Created At',
            'Phone'=>'Phone',
            'GenderCode'=>'Gender Code',
            'Earned' =>'Earned',
            'Unlocked' => 'Unlocked'

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPhotos()
    {
        return $this->hasMany(UserPhoto::className(), ['UserID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(UserStatus::className(), ['UserID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTags()
    {
        return $this->hasMany(UserTag::className(), ['UserID' => 'ID'])->with('tag');
    }

    public function getSettings()
    {
        return $this->hasOne(UserSettings::className(), ['UserID' => 'ID']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return User::find()->where(['ID' => $id])->one();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return User::findOne(['AuthKey' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return User::findOne(['UserName' => $username]);
    }

    public static function findByPhone($phone)
    {
        return User::findOne(['Phone' => $phone]);
    }

    public function getId()
    {
        return $this->ID;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->AuthKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->AuthKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->Password === md5($password);
    }

    public function beforeSave($insert) {
        unset($this->Photos);
        unset($this->Settings);
        unset($this->AccountStatus);
        unset($this->Tags);
        if ($this->Encrypt == true){
            $pass = md5($this->Password);
            $this->Password = $pass;
        }

        if ($this->isNewRecord){
            $this->AuthKey =  substr(hash('sha512', mt_rand()), 0, 25);
        }

        if ($this->Gender === 'Boy'){
            switch ($this->Interested) {
                case 'Boys':
                    $this->GenderCode = 8;
                    break;
                case 'Girls':
                    $this->GenderCode = 9;
                    break;
            }
        }elseif ($this->Gender === 'Girl'){
            switch ($this->Interested){
                case 'Boys':
                    $this->GenderCode = 6;
                    break;
                case 'Girls':
                    $this->GenderCode = 7;
                    break;
            }
        }
        return true;
    }

    public function afterFind() {
        parent::afterFind();
        $this->Photos = $this->userPhotos;
        $tags = [];
        foreach ($this->userTags as $ut){
            $tags[] = $ut->tag;
        }
        $this->Tags = $tags;
    }

    public function getMainPhoto(){
        return UserPhoto::find()->where(['UserID'=>$this->ID,'IsMain'=>1])->one();
    }

    public function getBlockedUsers(){
        return UserBlock::find()->where(['UserID'=>$this->ID])->select('BlockedUserID')->asArray()->all();
    }

    public function getEarned(){
        $earn = EarnCoin::find()->where(['UserID'=>$this->ID])->one();
        if ($earn) {
            $total = $earn->TotalEarned;
            $current = strtotime(date("Y-m-d"));
            $date = strtotime(date("Y-m-d", $earn->LastEarnTime));
            $datediff = $date - $current;
            $difference = $datediff ;
            if ($difference === 0) {
                $today = $earn->LastEarnCount;
            } else {
                $today = 0;
            }
        }else{
            $total = 0;
            $today = 0;
        }

        return ['total_earned'=>$total,'today_earned'=>$today];
    }

    public function getUnlockedIDS(){
        $ids =  UserUnlock::find()->where(['UserID'=>$this->ID])->select('UnlockedID')->asArray(true)->all();
        $response = [];
        foreach ($ids as $id) {
            $response[] = $id['UnlockedID'];
        }
        return $response;
    }
}
