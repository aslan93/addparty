<?php

namespace app\modules\User\models;

use Yii;

/**
 * This is the model class for table "UserStatus".
 *
 * @property int $ID
 * @property int $Coins
 * @property int $Boost
 * @property int $BoostedAt
 * @property string $ViewedMe
 * @property string $LikedMe
 * @property string $MyLikes
 * @property int $Likes
 * @property int $Vip
 * @property int $UserID
 *
 * @property User $user
 */
class UserStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'UserStatus';
    }
    public function attributes()
    {
        return array_merge(
            parent::attributes(),['ILiked','NewLikes','NewViews']

        );
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'UserID'], 'required'],
            [['Coins', 'Boost', 'BoostedAt', 'Likes', 'Views', 'Vip', 'UserID'], 'integer'],
            [['Coins', 'Boost', 'BoostedAt', 'ViewedMe', 'LikedMe', 'MyLikes', 'Likes', 'Vip','ILiked','NewLikes','NewViews'],'safe'],
            [['ViewedMe', 'LikedMe', 'MyLikes'], 'string'],
            [['UserID'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['UserID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Coins' => 'Coins',
            'Boost' => 'Boost',
            'BoostedAt' => 'Boosted At',
            'ViewedMe' => 'Viewed Me',
            'LikedMe' => 'Liked Me',
            'MyLikes' => 'My Likes',
            'Likes' => 'Likes',
            'Vip' => 'Vip',
            'UserID' => 'User ID',
            'Views'=>'Views',
            'ILiked'=>'ILiked',
            'NewLikes'=>'New Likes',
            'NewViews' => 'NewViews'
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'UserID']);
    }

    public function afterFind() {
        parent::afterFind();
        $boostedTime = new \DateTime(date('Y-m-d H:i:s',$this->BoostedAt));
        $currentTime = new \DateTime(date('Y-m-d H:i:s',time()));
        $interval = $currentTime->diff($boostedTime);
        $interval = $interval->format('%h');
        if ($interval >= $this->Boost){
            $this->Boost = 0;
            $this->BoostedAt = 0;
            $this->save();
        }elseif ($interval < $this->Boost && $interval >= 1){
            $this->Boost -= $interval;
            $this->BoostedAt = $this->BoostedAt + 60*60*$interval;
            $this->save();
        }
    }
}
