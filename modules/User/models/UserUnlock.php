<?php

namespace app\modules\User\models;

use Yii;

/**
 * This is the model class for table "UserUnlock".
 *
 * @property int $ID
 * @property int $UserID
 * @property int $UnlockedID
 *
 * @property User $unlocked
 * @property User $user
 */
class UserUnlock extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'UserUnlock';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UserID', 'UnlockedID'], 'required'],
            [['UserID', 'UnlockedID'], 'integer'],
            [['UnlockedID'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['UnlockedID' => 'ID']],
            [['UserID'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['UserID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'UserID' => 'User ID',
            'UnlockedID' => 'Unlocked ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnlocked()
    {
        return $this->hasOne(User::className(), ['ID' => 'UnlockedID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'UserID']);
    }
}
