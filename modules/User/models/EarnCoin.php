<?php

namespace app\modules\User\models;

use Yii;

/**
 * This is the model class for table "EarnCoin".
 *
 * @property int $ID
 * @property int $UserID
 * @property int $TotalEarned
 * @property int $LastEarnTime
 * @property int $LastEarnCount
 */
class EarnCoin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EarnCoin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UserID', 'TotalEarned', 'LastEarnTime', 'LastEarnCount'], 'required'],
            [['UserID', 'TotalEarned', 'LastEarnTime', 'LastEarnCount'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'UserID' => 'User ID',
            'TotalEarned' => 'Total Earned',
            'LastEarnTime' => 'Last Earn Time',
            'LastEarnCount' => 'Last Earn Count',
        ];
    }
}
