<?php

namespace app\modules\User\models;

use Yii;

/**
 * This is the model class for table "UserPhoto".
 *
 * @property int $ID
 * @property int $UserID
 * @property string $Large
 * @property string $Medium
 * @property string $Small
 * @property int $IsMain
 *
 * @property User $user
 */
class UserPhoto extends \yii\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'UserPhoto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UserID'], 'required'],
            [['UserID', 'IsMain'], 'integer'],
            [['Large', 'Medium', 'Small'], 'string', 'max' => 255],
            [['UserID'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['UserID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'UserID' => 'User ID',
            'Large' => 'Large',
            'Medium' => 'Medium',
            'Small' => 'Small',
            'IsMain' => 'Is Main',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'UserID']);
    }

    public function beforeDelete()
    {
        unlink(Yii::getAlias("@webroot" . '/uploads/users/' .$this->Large));
        unlink(Yii::getAlias("@webroot" . '/uploads/users/' .$this->Medium));
        unlink(Yii::getAlias("@webroot" . '/uploads/users/' .$this->Small));
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

}
