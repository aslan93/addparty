<?php

namespace app\modules\User\models;

use Yii;

/**
 * This is the model class for table "UserPhoto".
 *
 * @property int $ID
 * @property int $UserID
 * @property string $BlockedUserID

 *
 * @property User $user
 */
class UserBlock extends \yii\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'UserBlock';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UserID','BlockedUserID'], 'required'],
            [['UserID', 'BlockedUserID'], 'integer'],
            [['UserID'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['UserID' => 'ID']],
            [['BlockedUserID'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['BlockedUserID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'UserID' => 'User ID',
            'BlockedUserID' => 'Blocked User ID'

        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'UserID']);
    }

    public function getBlockedUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'BlockedUserID']);
    }

}
